<?php

session_start();
include 'functions.php';
include 'epriligo.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />
    <link rel="stylesheet" href="style/indexstyle.css" />
</head>
<body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container-fluid">
    <img src="style/img/madiunlogo.png" width="55" height="60" alt="" />
    <a class="navbar-brand" href="#">Pasar Besar Kota Madiun</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse text-right" id="navbarSupportedContent">
      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link " aria-current="page" href="index.php">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">News</a>
        </li>
        <li class="nav-item dropdown active">
          <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"> About </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item active" href="sejarah.html">Sejarah</a></li>
            <li><a class="dropdown-item" href="#">Fasilitas</a></li>
          
          </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link " aria-current="page" href="#">Contact</a>
        </li>
      </ul>
      <form class="d-flex">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </div>
  </div>
</nav>

<div class="blog" >
      <h1 class="my-5 text-center">Blog News</h1 >
      <div class="row row-cols-1 row-cols-md-4 g-3">
       <?php
         $query = mysqli_query($conn,"SELECT * FROM news ORDER BY id DESC LIMIT 8");
         while($f_widhy = mysqli_fetch_assoc($query)){
       ?>  
       <div class="col">
          <div class="card h-100">
            <img src="<?php echo $f_widhy['thumnail']; ?>" class="card-img-top" alt="..." height="200px">
            <div class="card-body">
              <h5 class="card-title"><?php echo $f_widhy['title']; ?></h5>
              <p class="card-text"><?php echo substr($f_widhy['artikel'], 0, 300); ?>
              <br>
              <a href="<?php echo $epriligo['base_url']; ?>read.php?id=<?php echo$f_widhy['id']; ?>">Read More</a>
              </p>
              
            </div>
            <div class="card-footer">
              <small class="text-muted"><?php echo$f_widhy['waktu']; ?></small>
            </div>
          </div>
        </div>
        <?php
         }
        ?>
            </div>
</body>
</html>