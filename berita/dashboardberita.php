<?php
  session_start();

  date_default_timezone_set('Asia/Jakarta');

  include '../functions.php';
  include '../epriligo.php';
 
  $waktu = date('Y-m-d H:i:s');

  if (isset($_POST['delete_data'])) {
    $post_id = $_POST['id'];

    if (empty($post_id)) {
        echo '<script>alert("Semua Field Tidak Boleh Di Kosongkan.");</script>';
    } else {
      $query_delete = mysqli_query($conn,"DELETE FROM news WHERE id = '$post_id'");
      echo '<script>alert("Data Berhasil Di Hapus Cuy!!!.");</script>';
    }
}

  if (isset($_POST['edit'])) {
    $post_id = $_POST['id'];
    $post_judul = $_POST['judul'];
    $post_artikel = $_POST['artikel'];

    if (empty($post_judul) || empty($post_artikel)) {
        echo '<script>alert("Semua Field Tidak Boleh Di Kosongkan.");</script>';
    } else {
        $targetDir = "";
        $targetFile = $targetDir . basename($_FILES["file"]["name"]);

        // Cek tipe file (jika hanya ingin menerima jpg, jpeg, png)
        $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
        if ($imageFileType != "jpg" && $imageFileType != "jpeg" && $imageFileType != "png") {
            echo "Maaf, hanya file JPG, JPEG, dan PNG yang diizinkan.";
        } else {
            // Coba unggah file
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $targetFile)) {
                $the_img = $epriligo['base_url'] .'berita/'. $targetFile;
                $del_then = mysqli_query($conn,"DELETE FROM news WHERE id = '$post_id'");
                $query = mysqli_query($conn, "INSERT INTO news (id,title, thumnail, artikel, waktu) VALUES ('$post_id','$post_judul', '$the_img', '$post_artikel', '$waktu')");
                echo "<script>alert('Data Berhasil Di Input');</script>";
            } else {
                echo "Maaf, terjadi kesalahan saat mengunggah file.";
            }
        }
    }
}
  

if (isset($_POST['simpan'])) {
    $post_judul = $_POST['judul'];
    $post_artikel = $_POST['artikel'];

    if (empty($post_judul) || empty($post_artikel)) {
        echo '<script>alert("Semua Field Tidak Boleh Di Kosongkan.");</script>';
    } else {
        $targetDir = "";
        $targetFile = $targetDir . basename($_FILES["file"]["name"]);

        // Cek tipe file (jika hanya ingin menerima jpg, jpeg, png)
        $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
        if ($imageFileType != "jpg" && $imageFileType != "jpeg" && $imageFileType != "png") {
            echo "Maaf, hanya file JPG, JPEG, dan PNG yang diizinkan.";
        } else {
            // Coba unggah file
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $targetFile)) {
                $the_img = $epriligo['base_url'] .'berita/'. $targetFile;
                $query = mysqli_query($conn, "INSERT INTO news (title, thumnail, artikel, waktu) VALUES ('$post_judul', '$the_img', '$post_artikel', '$waktu')");
                echo "<script>alert('Data Berhasil Di Input');</script>";
            } else {
                echo "Maaf, terjadi kesalahan saat mengunggah file.";
            }
        }
    }
}


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard Berita</title>
        <link href="../css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="index.html">Dashboard Admin</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search for..." aria-describedby="btnNavbarSearch" />
                    <button class="btn btn-primary" id="btnNavbarSearch" type="button"><i class="fas fa-search"></i></button>
                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#!">Settings</a></li>
                        <li><a class="dropdown-item" href="#!">Activity Log</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="../logout.php">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Core</div>
                            <a class="nav-link" href="index.html">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a>
                            <div class="sb-sidenav-menu-heading">Interface</div>
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                Layouts
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="./usercrud.php">Penguna Website</a>
                                    <a class="nav-link" href="./bahanpokokcrud.php">Bahan Pokok</a>
                                    <a class="nav-link" href="./dashboardChartKios.php">Jumlah Kios</a>
                                    <a class="nav-link" href="./berita/dashboardberita.php">Berita</a>
                                </nav>
                            </div>
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                Pages
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            
                            
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        Admin
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active">Sidenav Light</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                            <div class="container mt-5">
    
    <div class="contact-container">
    <h3>Menambahkan berita</h3><br>
        <form action="" method="POST" enctype="multipart/form-data">
        <div class="form-group">
           <label>Judul</label>
           <input type="text" class="form-control" name="judul"></input>
        </div>
        <div class="form-group">
           <label>Thumnail</label>
           <input type="file" class="form-control" name="file"></input>
        </div>
        <div class="form-group">
          <label>Artikel</label>
          <textarea name="artikel" id="editor1" class="form-control" rows="5"></textarea>
        </div>
        <button type="submit" name="simpan" class="btn btn-success mt-3" style="width: 100%;">Submit</button>
        </form>
    </div>
  </div>

  <div class="container mt-5">
    <div class="contact-container">
      <h3>Mengedit berita</h3>
        <form action="" method="POST" enctype="multipart/form-data">
        <div class="form-group">
           <label>ID</label>
           <select name="id" class="form-control">
             <option value="0">Pilih</option>
             <?php
               $query = mysqli_query($conn,"SELECT * FROM news");
               while($fetch = mysqli_fetch_assoc($query)){
             ?>
               <option value="<?php echo $fetch['id']; ?>"><?php echo $fetch['title']; ?> (<?php echo $fetch['id']; ?>)</option> 
             <?php
               } 
            ?>
           </select>
        </div>
        <div class="form-group">
           <label>Judul</label>
           <input type="text" class="form-control" name="judul"></input>
        </div>
        <div class="form-group">
           <label>Thumnail</label>
           <input type="file" class="form-control" name="file"></input>
        </div>
        <div class="form-group">
          <label>Artikel</label>
          <textarea name="artikel" id="editor1" class="form-control" rows="5"></textarea>
        </div>
        <button type="submit" name="edit" class="btn btn-success mt-3" style="width: 100%;">Submit</button>
        </form>
    </div>
  </div>

  <div class="container mt-5">
    <div class="contact-container">
        <form action="" method="POST" enctype="multipart/form-data">
            <h3>Hapus berita</h3>
        <div class="form-group">
           <label>ID</label>
           <select name="id" class="form-control">
             <option value="0">Pilih</option>
             <?php
               $query = mysqli_query($conn,"SELECT * FROM news");
               while($fetch = mysqli_fetch_assoc($query)){
             ?>
               <option value="<?php echo $fetch['id']; ?>"><?php echo $fetch['title']; ?> (<?php echo $fetch['id']; ?>)</option> 
             <?php
               } 
            ?>
           </select>
        </div>
        <button type="submit" name="delete_data" class="btn btn-success mt-3" style="width: 100%;">Submit</button>
        </form>
    </div>
  </div>

  <!-- Bootstrap JS and dependencies (for certain components) -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <script>
    <script>
    ClassicEditor
      .create(document.querySelector('#editor1'))
      .catch(error => {
        console.error(error);
      });
      </script>
  </script>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2023</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>
