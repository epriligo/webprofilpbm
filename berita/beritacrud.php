<?php
  session_start();

  date_default_timezone_set('Asia/Jakarta');

  include '../functions.php';
  include '../epriligo.php';
 
  $waktu = date('Y-m-d H:i:s');

  if (isset($_POST['delete_data'])) {
    $post_id = $_POST['id'];

    if (empty($post_id)) {
        echo '<script>alert("Semua Field Tidak Boleh Di Kosongkan.");</script>';
    } else {
      $query_delete = mysqli_query($conn,"DELETE FROM news WHERE id = '$post_id'");
      echo '<script>alert("Data Berhasil Di Hapus Cuy!!!.");</script>';
    }
}

  if (isset($_POST['edit'])) {
    $post_id = $_POST['id'];
    $post_judul = $_POST['judul'];
    $post_artikel = $_POST['artikel'];

    if (empty($post_judul) || empty($post_artikel)) {
        echo '<script>alert("Semua Field Tidak Boleh Di Kosongkan.");</script>';
    } else {
        $targetDir = "";
        $targetFile = $targetDir . basename($_FILES["file"]["name"]);

        // Cek tipe file (jika hanya ingin menerima jpg, jpeg, png)
        $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
        if ($imageFileType != "jpg" && $imageFileType != "jpeg" && $imageFileType != "png") {
            echo "Maaf, hanya file JPG, JPEG, dan PNG yang diizinkan.";
        } else {
            // Coba unggah file
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $targetFile)) {
                $the_img = $epriligo['base_url'] .'berita/'. $targetFile;
                $del_then = mysqli_query($conn,"DELETE FROM news WHERE id = '$post_id'");
                $query = mysqli_query($conn, "INSERT INTO news (id,title, thumnail, artikel, waktu) VALUES ('$post_id','$post_judul', '$the_img', '$post_artikel', '$waktu')");
                echo "<script>alert('Data Berhasil Di Input');</script>";
            } else {
                echo "Maaf, terjadi kesalahan saat mengunggah file.";
            }
        }
    }
}
  

if (isset($_POST['simpan'])) {
    $post_judul = $_POST['judul'];
    $post_artikel = $_POST['artikel'];

    if (empty($post_judul) || empty($post_artikel)) {
        echo '<script>alert("Semua Field Tidak Boleh Di Kosongkan.");</script>';
    } else {
        $targetDir = "";
        $targetFile = $targetDir . basename($_FILES["file"]["name"]);

        // Cek tipe file (jika hanya ingin menerima jpg, jpeg, png)
        $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
        if ($imageFileType != "jpg" && $imageFileType != "jpeg" && $imageFileType != "png") {
            echo "Maaf, hanya file JPG, JPEG, dan PNG yang diizinkan.";
        } else {
            // Coba unggah file
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $targetFile)) {
                $the_img = $epriligo['base_url'] .'berita/'. $targetFile;
                $query = mysqli_query($conn, "INSERT INTO news (title, thumnail, artikel, waktu) VALUES ('$post_judul', '$the_img', '$post_artikel', '$waktu')");
                echo "<script>alert('Data Berhasil Di Input');</script>";
            } else {
                echo "Maaf, terjadi kesalahan saat mengunggah file.";
            }
        }
    }
}


?>

<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <title>Admin</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
  <!-- Font Awesome CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
  <script src="https://cdn.ckeditor.com/ckeditor5/40.1.0/classic/ckeditor.js"></script>
  <style>
    /* Custom CSS */
    body {
      font-family: Arial, sans-serif;
      background-color: #f4f4f4;
    }
    .contact-container {
      max-width: 700px;
      margin: 0 auto;
      padding: 30px;
      background-color: #fff;
      border-radius: 8px;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    }
    .contact-info {
      list-style: none;
      padding: 0;
    }
    .contact-info li {
      margin-bottom: 20px;
      display: flex;
      align-items: center;
    }
    .contact-info li i {
      font-size: 24px;
      margin-right: 10px;
      color: #007bff;
    }
    .map-responsive {
      overflow: hidden;
      padding-bottom: 56.25%; /* Ratio 16:9 */
      position: relative;
      height: 0;
    }
    .map-responsive iframe {
      left: 0;
      top: 0;
      height: 100%;
      width: 100%;
      position: absolute;
    }
  </style>
</head>
<body>
  <div class="container mt-5">
    
    <div class="contact-container">
    <h3>Menambahkan berita</h3><br>
        <form action="" method="POST" enctype="multipart/form-data">
        <div class="form-group">
           <label>Judul</label>
           <input type="text" class="form-control" name="judul"></input>
        </div>
        <div class="form-group">
           <label>Thumnail</label>
           <input type="file" class="form-control" name="file"></input>
        </div>
        <div class="form-group">
          <label>Artikel</label>
          <textarea name="artikel" id="editor1" class="form-control" rows="5"></textarea>
        </div>
        <button type="submit" name="simpan" class="btn btn-success mt-3" style="width: 100%;">Submit</button>
        </form>
    </div>
  </div>

  <div class="container mt-5">
    <div class="contact-container">
      <h3>Mengedit berita</h3>
        <form action="" method="POST" enctype="multipart/form-data">
        <div class="form-group">
           <label>ID</label>
           <select name="id" class="form-control">
             <option value="0">Pilih</option>
             <?php
               $query = mysqli_query($conn,"SELECT * FROM news");
               while($fetch = mysqli_fetch_assoc($query)){
             ?>
               <option value="<?php echo $fetch['id']; ?>"><?php echo $fetch['title']; ?> (<?php echo $fetch['id']; ?>)</option> 
             <?php
               } 
            ?>
           </select>
        </div>
        <div class="form-group">
           <label>Judul</label>
           <input type="text" class="form-control" name="judul"></input>
        </div>
        <div class="form-group">
           <label>Thumnail</label>
           <input type="file" class="form-control" name="file"></input>
        </div>
        <div class="form-group">
          <label>Artikel</label>
          <textarea name="artikel" id="editor1" class="form-control" rows="5"></textarea>
        </div>
        <button type="submit" name="edit" class="btn btn-success mt-3" style="width: 100%;">Submit</button>
        </form>
    </div>
  </div>

  <div class="container mt-5">
    <div class="contact-container">
        <form action="" method="POST" enctype="multipart/form-data">
        <div class="form-group">
           <label>ID</label>
           <select name="id" class="form-control">
             <option value="0">Pilih</option>
             <?php
               $query = mysqli_query($conn,"SELECT * FROM news");
               while($fetch = mysqli_fetch_assoc($query)){
             ?>
               <option value="<?php echo $fetch['id']; ?>"><?php echo $fetch['title']; ?> (<?php echo $fetch['id']; ?>)</option> 
             <?php
               } 
            ?>
           </select>
        </div>
        <button type="submit" name="delete_data" class="btn btn-success mt-3" style="width: 100%;">Submit</button>
        </form>
    </div>
  </div>

  <!-- Bootstrap JS and dependencies (for certain components) -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <script>
    <script>
    ClassicEditor
      .create(document.querySelector('#editor1'))
      .catch(error => {
        console.error(error);
      });
      </script>
  </script>
</body>
</html>

