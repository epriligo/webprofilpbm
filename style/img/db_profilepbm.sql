-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 22 Nov 2023 pada 14.55
-- Versi server: 10.4.25-MariaDB
-- Versi PHP: 8.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_profilepbm`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bahanpokok`
--

CREATE TABLE `bahanpokok` (
  `id` int(155) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `harga` int(120) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `bahanpokok`
--

INSERT INTO `bahanpokok` (`id`, `nama`, `harga`, `deskripsi`, `img`) VALUES
(2, 'gandum', 34400, '', 'gandum.jpg'),
(3, 'minyak', 35000, 'ini minyak', 'minyak.jpg'),
(6, 'gula', 20000, 'ini gula', 'gula.jpg'),
(7, 'Bawang putih', 15000, 'fds', 'mie.jpg'),
(8, 'terigu', 19000, '', 'terigu.jpg'),
(12, 'Beras Putih', 15000, 'harga perkilo', 'beras.jpg'),
(13, 'Caber rawit', 50000, 'untuk 1kg', 'caberawit.jpg'),
(14, 'Daging Ayam', 30000, 'Untuk 1kg', 'dagingayam.jpg'),
(15, 'Tepung TErigu', 20000, 'untuk 1 kg', 'terigu.jpg'),
(16, 'Telur ayam', 27000, 'harga perkilo', 'telur.jpg'),
(17, 'Garam', 10000, '', 'garam.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tagihan`
--

CREATE TABLE `tagihan` (
  `id` int(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kode_kios` varchar(255) NOT NULL,
  `tahun` varchar(255) NOT NULL,
  `jumlah_tagihan` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tagihan`
--

INSERT INTO `tagihan` (`id`, `nama`, `alamat`, `kode_kios`, `tahun`, `jumlah_tagihan`) VALUES
(9, 'yani', 'ds driyorejo', 't113', '2020 - 2021', 200000),
(10, 'Nur Handayani', 'Kec. Ngeger', 'L/A1/PBM', '2020 - 2024', 8000000),
(13, 'Endang Puryati', 'kec. Dolopo', 'L/B1/PBM', '2020 -2021', 2100000),
(15, 'Sulistian', 'Kec.Nguntoronadi Magetan', 'L/B3/PBM', '2021 - 2024', 4100000),
(21, 'Waginem', 'kec.Demagan', 'L/B5/PBM', '2023', 2000000),
(22, 'Heru Wahyudi', 'Kec.Panekan Magetan', 'K/A1/PBM', '2023', 2000000),
(23, 'Instanti', 'Kec. Parang', 'K/B4/PBM', '2022-2023', 4000000),
(24, 'Suyadi', 'Kec. Tamanan', 'L/D1/PBM', '2021-2023', 6000000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `kode_kios` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(122) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `kode_kios`, `email`, `password`, `level`) VALUES
(5, 'yani', '', 'yani@gmail.com', '$2y$10$kLuyt8nY23zkQ/MxmR.TlO5dY1ehhUb1D7chg2E0Gh0AGNwhEwQZ2', 'admin'),
(10, 'adel', 't113', 'adella@gmail.com', '$2y$10$BuN64.fFqxPQ3Cm60a1DCOF7n0D.Ly5vdIo2ZltB.9LsaWZQrKEcu', 'user'),
(11, 'lewandoski', 't115', 'lewandoski@gmail.comrt', '$2y$10$uLjL1H2ubyQYDdX15UUGLuCE.6yoV5ZjckMkoekpUyltgMG16XmFS', 'admin'),
(13, 'aldo', 'AAV112', 'aldo123@gmail.com', '$2y$10$MtE8GIT9UnxSqXGwjfvbyugt/UafaRLRERx6tHm2fhLj6.eTKx6fi', 'user'),
(15, 'fero', 'trr243', 'dsfgh', '$2y$10$y/2AiU02HSTK9OngCulk9O5gFQCYxY7iwc3GIcqdIydWoL/zHy03O', 'admin'),
(20, 'admin', 'admin', 'admin@gmail.com', '$2y$10$uhRc35Fl4PldaPYBoEWoKeHICabva5VN4jDopeIZ51RaCTIqNM4vi', 'admin'),
(21, 'adella', 't112', 'adel@gmail.com', '$2y$10$WUNB.vNEpJrDgZ51FP80dei.xhuG7qEE1Jl8g6Q6zw0.DdpiG9xzy', 'admin'),
(22, 'tes', 'tes123', 'tes@gmail', '$2y$10$/q1jwb0of0wQEPF0LE.abOX4g7F7XAWMwiRi2x83YWOW4wWNUAXt6', 'user');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `bahanpokok`
--
ALTER TABLE `bahanpokok`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tagihan`
--
ALTER TABLE `tagihan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `bahanpokok`
--
ALTER TABLE `bahanpokok`
  MODIFY `id` int(155) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `tagihan`
--
ALTER TABLE `tagihan`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
