<?php
include ("functionbahanpokok.php");
$produk = query("SELECT * FROM bahanpokok ");
// $produk = mysqli_fetch_assoc($rsltproduk);

//search query 

?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />
    <link rel="stylesheet" href="style/indexstyle.css" />
  </head>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
      <div class="container-fluid">
        <img src="style/img/madiunlogo.png" width="55" height="60" alt="" />
        <a class="navbar-brand" href="#">Pasar Besar Kota Madiun</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse text-right" id="navbarSupportedContent">
          <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link " aria-current="page" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">News</a>
            </li>
            <li class="nav-item dropdown active">
              <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"> About </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item active" href="sejarah.html">Sejarah</a></li>
                <li><a class="dropdown-item" href="#">Fasilitas</a></li>
              
              </ul>
            </li>
            <li class="nav-item">
              <a class="nav-link " aria-current="page" href="#">Contact</a>
            </li>
          </ul>
          <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
            <button class="btn btn-outline-success" type="submit">Search</button>
          </form>
        </div>
      </div>
    </nav>
    <div class="row mb-2" >
        <?php foreach($produk as $row) : ?>
      <div class="col-md-2 mb-2" >
        <div class="card">
          <img src="style/imgbahanpokok/<?php echo $row["img"]; ?>" class="card-img-top" alt="..." height="200px" />
          <div class="card-body">
            <h5 class="card-title"><?php echo $row["nama"];?></h5>
            <p class="card-text">Rp. <?php echo $row["harga"];?></p>
            <a href="404.html" class="btn btn-primary">Lihat Disini</a>
          </div>
        </div>
      </div>
      <?php endforeach; ?>
    </div>





    <footer>
      <div class="main-content">
        <div class="left box">
          <h2>About us</h2>
          <div class="content">
            <p>Dinas Perdagangan Kota adalah sebuah lembaga pemerintah daerah yang bertanggung jawab atas pengelolaan dan pengembangan sektor perdagangan di suatu kota. Tugas utama Dinas Perdagangan Kota meliputi pemantauan dan pengawasan aktivitas perdagangan, pemenuhan kebijakan pemerintah terkait perdagangan, pengaturan izin usaha, promosi perdagangan lokal, serta perlindungan konsumen.</p>
            <div class="social">
              <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
              <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
              <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
              <a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></span></a>
            </div>
          </div>
        </div>
        <div class="center box">
          <h2>Quick Links</h2>
          <div class="content">
          <ul class="box">
            
            <li><a href="">About</a></li>
            <li><a href="#">FaQ</a></li>
            <li><a href="#">Help</a></li>
            <li><a href="#">Term & Conditions</a></li>
            <li><a href="">Privacy</a></li>
          </ul>
          </div>
        </div>
        <div class="right box">
          <h2>Contact us</h2>
          <div class="content">
            <form  method="post" action="kirim.php">
              <div class="email">
                <div class="text">Email *</div>
                <input type="email" name="email" required>
              </div>
              <div class="msg">
                <div class="text">Message *</div>
                <textarea rows="2" cols="25" name="message"required></textarea>
              </div>
              <div class="btn">
                <button type="submit" name="sentemail">Send</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="bottom">
        <center>
          <!-- <span class="credit">Created By <a href="#"></a> | </span> -->
          <i class="fa fa-copyright" aria-hidden="true"></i><span> 2022 All rights reserved.</span>
        </center>
      </div>
    </footer>

  </body>
</html>
