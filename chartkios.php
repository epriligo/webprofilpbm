<?php
include("functionchart.php");
?>


<!DOCTYPE html>
<html>
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />
    <link rel="stylesheet" href="style/indexstyle.css" />
    <title>Chart Pie Dinamis dengan PHP dan MySQL</title>
    
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }
        .chart-container {
            position: relative;
            margin: auto;
            height: 400px;
            width: 400px;
        }
        .legend-container {
            text-align: center;
            margin-top: 20px;
        }
        .legend-color {
            display: inline-block;
            width: 20px;
            height: 20px;
            margin-right: 5px;
            vertical-align: middle;
        }
    </style>
</head>
<body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container-fluid">
    <img src="style/img/madiunlogo.png" width="55" height="60" alt="" />
    <a class="navbar-brand" href="#">Pasar Besar Kota Madiun</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse text-right" id="navbarSupportedContent">
      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link " aria-current="page" href="index.html">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">News</a>
        </li>
        <li class="nav-item dropdown active">
          <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"> About </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item active" href="sejarah.html">Sejarah</a></li>
            <li><a class="dropdown-item" href="#">Fasilitas</a></li>
          
          </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link " aria-current="page" href="#">Contact</a>
        </li>
      </ul>
      <form class="d-flex">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </div>
  </div>
</nav>


    <div class="chart-container">
        <canvas id="myPieChart"></canvas>
        <div id="legend" class="legend-container"></div>
    </div>

    <script>
        var data = {
            labels: ['Julmah Kios Terisi', 'Jumlah Kios Kosong'],
            datasets: [{
                data: [<?php echo $data['jumlah_terisi']; ?>, <?php echo $data['jumlah_kosong']; ?>],
                backgroundColor: ['rgba(75, 192, 192, 0.2)', 'rgba(255, 99, 132, 0.2)'],
                borderColor: ['rgba(75, 192, 192, 1)', 'rgba(255, 99, 132, 1)'],
                borderWidth: 1
            }]
        };

        var ctx = document.getElementById('myPieChart').getContext('2d');
        var myPieChart = new Chart(ctx, {
            type: 'pie',
            data: data,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false
                }
            }
        });

        var legend = document.getElementById('legend');
        var labels = data.labels;
        var datasets = data.datasets[0];

        labels.forEach(function(label, index) {
            var div = document.createElement('div');
            div.innerHTML = `<span class="legend-color" style="background-color: ${datasets.backgroundColor[index]}"></span><span class="legend-label">${label}: ${datasets.data[index]}</span>`;
            legend.appendChild(div);
        });
    </script>
</body>
</html>
