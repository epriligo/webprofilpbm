<?php
session_start();
include 'functions.php';
include 'epriligo.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />
    <link rel="stylesheet" href="style/indexstyle.css" />
  </head>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
      <div class="container-fluid">
        <img src="style/img/madiunlogo.png" width="55" height="60" alt="" />
        <a class="navbar-brand" href="#">Pasar Besar Kota Madiun</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse text-right" id="navbarSupportedContent">
          <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="#">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="news.php">News</a>
            </li>
            <a class="nav-link" aria-current="page" href="sejarah.html">About</a>
            <!-- <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"> About </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="sejarah.html">Sejarah</a></li>
                <li><a class="dropdown-item" href="fasilitas.html">Fasilitas</a></li>
              </ul> -->
            </li>
            <li class="nav-item">
              <a class="nav-link" aria-current="page" href="contact.html">Contact</a>
            </li>
          </ul>
          <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
            <button class="btn btn-outline-success" type="submit">Search</button>
          </form>
        </div>
      </div>
    </nav>
    <!-- banner -->
    <div class="banner">
      <div class="container text-center">
        <h4 class="display-6">Selamat Datang di Website Kami</h4>
        <h3 class="display-1">Pasar Besar Kota Madiun</h3>
        <!-- <a href="tagihanuser.php">
          <button type="button" class="btn btn-danger btn-lg">Cek Layanan</button>
        </a> -->
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="card">
          <img src="style/img/bahan.png" class="card-img-top" alt="..." height="200px" />
          <div class="card-body">
            <h5 class="card-title">Bahan Pokok</h5>
            <p class="card-text">Tersedia berbagai macam bahan pokok seperti beras,gula,minyak,telur dll. Anda ingin melihat update harga.</p>
            <a href="bahanpokok.php" class="btn btn-primary">Lihat Disini</a>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card">
          <img src="style/img/lapak.jpg" class="card-img-top" alt="..." height="200px" width="100px"/>
          <div class="card-body">
            <h5 class="card-title">Informasi Kios/Los</h5>
            <p class="card-text">Terdapat beberapa ratusan kios dan los yang bisa digunakan untuk berdagang. Cek jumlah kios/los</p>
            <a href="chartkios.php" class="btn btn-primary">Lihat Disini</a>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card">
          <img src="style/img/galery.jpg" class="card-img-top" alt="..." height="220px" />
          <div class="card-body">
            <h5 class="card-title">Galery Foto</h5>
            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
            <a href="galeryfoto.html" class="btn btn-primary">Lihat Disini</a>
          </div>
        </div>
      </div>

    
    </div>
    <div class="wrapper">
      <section id="home">
          <img src="style/img/frestfruit.jpg" width="45%"/>
          <div class="kolom">
              <p class="deskripsi">"Ayo Belanja ke Pasar Tradisional"</p>
              <h2 class="destxt">"Tersedia segala barang,bahan pokok, pakaian, makanan; grosir lengkap murah meriah</h2>
              <p>Pasar tradisional adalah jantung budaya dan kehidupan sehari-hari di banyak negara di seluruh dunia. Mereka bukan sekadar tempat berbelanja, melainkan juga tempat berbagi cerita, menghormati warisan leluhur, dan merayakan keanekaragaman. Di pasar tradisional, kita dapat merasakan sejuknya sejarah yang terpelihara dan merasakan keaslian yang mungkin sulit ditemukan di tempat lain. Dengan berbagai penjual yang menjajakan beragam produk, dari makanan lokal yang lezat hingga kerajinan tangan yang unik, pasar tradisional adalah tempat yang memancarkan kehangatan keramahtamahan dan menyatukan komunitas. Mereka juga mendukung ekonomi lokal dengan memberdayakan para pedagang kecil dan menghubungkan pembeli dengan produk lokal berkualitas tinggi. Dengan semua keunikan dan pesona pasar tradisional ini, mereka tetap menjadi bagian penting dari identitas budaya dan kehidupan sehari-hari masyarakat.</p>
              
          </div>
      </section>
  <!-- blog news -->
    <div class="blog">
      <h1 class="my-5 text-center">Latest Blog News</h1 >
      <div class="row row-cols-1 row-cols-md-3 g-4">
      <?php
         $query = mysqli_query($conn,"SELECT * FROM news ORDER BY id DESC LIMIT 3");
         while($f_widhy = mysqli_fetch_assoc($query)){
       ?>  
       <div class="col">
          <div class="card h-100">
            <img src="<?php echo $f_widhy['thumnail']; ?>" class="card-img-top" alt="..." height="200px">
            <div class="card-body">
              <h5 class="card-title"><?php echo $f_widhy['title']; ?></h5>
              <p class="card-text"><?php echo substr($f_widhy['artikel'], 0, 300); ?>
              <br>
              <a href="<?php echo $epriligo['base_url']; ?>read.php?id=<?php echo$f_widhy['id']; ?>">Read More</a>
              </p>
              
            </div>
            <div class="card-footer">
              <small class="text-muted"><?php echo$f_widhy['waktu']; ?></small>
            </div>
          </div>
        </div>
        <?php
         }
        ?>
        
    </div>  
    <!-- footer -->
    <footer>
      <div class="main-content">
        <div class="left box">
          <h2>About us</h2>
          <div class="content">
            <p>Dinas Perdagangan Kota adalah sebuah lembaga pemerintah daerah yang bertanggung jawab atas pengelolaan dan pengembangan sektor perdagangan di suatu kota. Tugas utama Dinas Perdagangan Kota meliputi pemantauan dan pengawasan aktivitas perdagangan, pemenuhan kebijakan pemerintah terkait perdagangan, pengaturan izin usaha, promosi perdagangan lokal, serta perlindungan konsumen.</p>
            <div class="social">
              <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
              <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
              <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
              <a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></span></a>
            </div>
          </div>
        </div>
        <div class="center box">
          <h2>Quick Links</h2>
          <div class="content">
          <ul class="box">
            
            <li><a href="">About</a></li>
            <li><a href="#">FaQ</a></li>
            <li><a href="#">Help</a></li>
            <li><a href="#">Term & Conditions</a></li>
            <li><a href="">Privacy</a></li>
          </ul>
          </div>
        </div>
        <div class="right box">
          <h2>Contact us</h2>
          <div class="content">
            <form  method="post" action="kirim.php">
              <div class="email">
                <div class="text">Email *</div>
                <input type="email" name="email" required>
              </div>
              <div class="msg">
                <div class="text">Message *</div>
                <textarea rows="2" cols="25" name="message"required></textarea>
              </div>
              <div class="btn">
                <button type="submit" name="sentemail">Send</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="bottom">
        <center>
          <!-- <span class="credit">Created By <a href="#"></a> | </span> -->
          <i class="fa fa-copyright" aria-hidden="true"></i><span> 2023 All rights reserved.</span>
        </center>
      </div>
    </footer>

  </body>
</html>
